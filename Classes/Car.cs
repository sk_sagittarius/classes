﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes
{
    class Car
    {
        private string mark;
        private int maxSpeed;
        private string color;
        private bool newCar;
        private float volume;
        private static int counter = 0;
        private static int some_number;

        public string Mark
        {
            get { return mark; }
            set { mark = value; }
        }

        public int MaxSpeed
        {
            get { return maxSpeed; }
            set { maxSpeed = value; }
        }

        public string Color
        {
            get { return color; }
            set { color = value; }
        }

        public bool NewCar
        {
            get { return newCar; }
            set { newCar = value; }
        }

        public float Volume
        {
            get { return volume; }
            set { volume = value; }
        }

        public void GetInfo()
        {
            Console.WriteLine($"Марка: {Mark},\nMax speed: {MaxSpeed},\nColor: {Color},\nNew car: {newCar},\nVolume: {Volume}");
        }

        public Car()
        {
            mark = "unknown";
            maxSpeed = 0;
            color = "unknown";
            newCar = false;
            volume = 0;
            counter++;
        }

        static Car()
        {
            some_number = 0;
        }
        public Car(string _mark)
        {
            mark = _mark;
            maxSpeed = 0;
            color = "unknown";
            newCar = false;
            volume = 0;
            counter++;
        }

        public Car(string _mark, string _color, bool _gasolintype)
        {
            mark = _mark;
            color = _color;
            newCar = _gasolintype;
            counter++;
        }


        public static void ShowCounter()
        {
            Console.WriteLine($"Создано {counter} объектов");
        }
    }
}
