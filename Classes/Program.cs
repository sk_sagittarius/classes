﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car
            {
                Mark = "Toyota",
                MaxSpeed = 100,
                Color = "black",
                NewCar = true,
                Volume = 3.3F
            };

            car.GetInfo();
            Console.WriteLine("\n");

            Car car2 = new Car();
            car2.GetInfo();
            Console.WriteLine("\n");

            Car car3 = new Car("Jeep");
            car3.GetInfo();
            Console.WriteLine("\n");

            Car car4 = new Car("Mazda", "red", true);
            car4.GetInfo();
            Console.WriteLine("\n");

            Car car5 = new Car("Porshe", "silver", true);
            car5.GetInfo();
            Console.WriteLine("\n");

            Car.ShowCounter();



            Console.ReadLine();
        }
    }
}
